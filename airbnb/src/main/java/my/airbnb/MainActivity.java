package my.airbnb;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, MainFragment.OnFragmentInteractionListener, MapInteractionListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private static class State implements Parcelable {
        public String currentTab;
        public ArrayList<MarkerOptions> markers = new ArrayList<MarkerOptions>();
        public LatLng mapPosition;
        public Float zoom;
        public LatLngBounds latLngBounds;

        public State() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.currentTab);
            dest.writeList(this.markers);
            dest.writeParcelable(this.mapPosition, flags);
            dest.writeValue(this.zoom);
        }

        private State(Parcel in) {
            this.currentTab = in.readString();
            this.markers = new ArrayList<MarkerOptions>();
            in.readList(this.markers, null);
            this.mapPosition = in.readParcelable(LatLng.class.getClassLoader());
            this.zoom = (Float) in.readValue(Float.class.getClassLoader());
        }

        public static Parcelable.Creator<State> CREATOR = new Parcelable.Creator<State>() {
            public State createFromParcel(Parcel source) {
                return new State(source);
            }

            public State[] newArray(int size) {
                return new State[size];
            }
        };
    }

    State state = new State();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("state", state);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        state = savedInstanceState.getParcelable("state");
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Log.e("----------", "onNavigationDrawerItemSelected " + position);

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (position != 0) {
            fragmentManager.beginTransaction().replace(R.id.container, PlaceholderFragment.newInstance(position + 1)).commit();
        } else {
            MainFragment f = (MainFragment) fragmentManager.findFragmentByTag("main");
            if (f == null) {
                f = MainFragment.newInstance();
            }
            fragmentManager.beginTransaction().replace(R.id.container, f, "main").commit();
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public List<MarkerOptions> getMarkers() {
        return state.markers;
    }

    @Override
    public List<MarkerOptions> getVisibleMarkers() {
        if (state.latLngBounds == null) return new ArrayList<MarkerOptions>(state.markers);
        List<MarkerOptions> res = new ArrayList<MarkerOptions>();
        for (MarkerOptions m : state.markers) {
            if (state.latLngBounds.contains(m.getPosition())) res.add(m);
            Log.e("", state.latLngBounds.toString() + " " + m.getPosition());
        }
        return res;
    }

    @Override
    public void setMapPosition(LatLng target, float zoom, LatLngBounds latLngBounds) {
        state.mapPosition = target;
        state.zoom = zoom;
        state.latLngBounds = latLngBounds;
    }

    @Override
    public LatLng getMapPosition() {
        return state.mapPosition;
    }

    @Override
    public Float getZoom() {
        return state.zoom;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMarkerListItemClicked(MarkerOptions id) {

    }

    @Override
    public void onNewMarker(MarkerOptions marker) {
        state.markers.add(marker);
    }

    @Override
    public void setCurrentTab(String tab) {
        state.currentTab = tab;
    }

    @Override
    public String getCurrentTab() {
        return state.currentTab;
    }
}

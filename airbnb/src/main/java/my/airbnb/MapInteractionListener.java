package my.airbnb;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;


public interface MapInteractionListener {

    void onNewMarker(MarkerOptions marker);

    void onMarkerListItemClicked(MarkerOptions id);

    List<MarkerOptions> getMarkers();

    void setMapPosition(LatLng target, float zoom, LatLngBounds latLngBounds);

    LatLng getMapPosition();

    Float getZoom();

    List<MarkerOptions> getVisibleMarkers();
}

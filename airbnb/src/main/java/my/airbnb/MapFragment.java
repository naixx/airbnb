package my.airbnb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapInteractionListener} interface
 * to handle interaction events.
 */
public class MapFragment extends SupportMapFragment {

    static int counter = 0;

    private MapInteractionListener mListener;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MapInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (getMap() == null) return; // if there are no Play Services
        getMap().setMyLocationEnabled(true);
        for (MarkerOptions m : mListener.getMarkers()) {
            getMap().addMarker(m);
        }
        getMap().setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                MarkerOptions mo = new MarkerOptions().position(latLng).title(String.valueOf(counter++));

                getMap().addMarker(mo).showInfoWindow();
                mListener.onNewMarker(mo);
            }
        });
        getMap().setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                mListener.setMapPosition(cameraPosition.target, cameraPosition.zoom, getMap().getProjection().getVisibleRegion().latLngBounds);
            }
        });

        if (mListener.getMapPosition() != null)
            getMap().moveCamera(CameraUpdateFactory.newLatLng(mListener.getMapPosition()));
        if (mListener.getZoom() != null)
            getMap().moveCamera(CameraUpdateFactory.zoomTo(mListener.getZoom()));


    }

}
